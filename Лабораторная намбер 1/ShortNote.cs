﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лабораторная_намбер_1
{
    public class ShortNote
    {
        public string Surname { get; private set; }
        public string Name { get; private set; }
        public long Number { get; private set; }
        public ShortNote(Note note)
        {
            this.Surname = note.Surname;
            this.Name = note.Name;
            this.Number = note.Number;
        }
        public override string ToString()
        {
            return Surname + " " + Name + " " + Number;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лабораторная_намбер_1
{
    class Interaction
    {
        public static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.GetEncoding(1251);
            Console.InputEncoding = Encoding.GetEncoding(1251);
            Notebook notebook = new Notebook();
            Console.WriteLine("Привет! Это блокнот. Введдите команду: 'Новая запись' - добавление новой записи, 'Выйти' - выйти из блокнота");
            while (true)
            {
                Console.WriteLine("После того, как ввели записи, введите команду 'Редактировать запись', 'Удалить запись', 'Показать запись', для соответствующих действий.");
                Console.WriteLine("Введите команду 'Вывести все' для просмотра всех записей.");
                string command = Console.ReadLine();
                switch (command)
                {
                    case "Выйти":
                        return;
                    case "Новая запись":
                        Console.WriteLine("Введите имя: ");
                        string name = Console.ReadLine();
                        Console.WriteLine("Введите фамилию: ");
                        string surname = Console.ReadLine();
                        Console.WriteLine("Введите номер телефона: ");
                        long number = Convert.ToInt64(Console.ReadLine());
                        Console.WriteLine("Введите страну: ");
                        string country = Console.ReadLine();
                        Console.WriteLine("Введите страницу: ");
                        int id_1 = Convert.ToInt32(Console.ReadLine());
                        Note note_1 = new Note(surname, name, number, country);
                        Console.WriteLine("Указать отчество? Введите 'Да' или 'Нет'");
                        switch (Console.ReadLine())
                        {
                            case "Да":
                                string patronymic = Console.ReadLine();
                                note_1.Patronymic = patronymic;
                                break;
                            case "Нет":
                                break;
                            default:
                                break;
                        }
                        Console.WriteLine("Указать организацию? Введите 'Да' или 'Нет'");
                        switch (Console.ReadLine())
                        {
                            case "Да":
                                string organization = Console.ReadLine();
                                note_1.Organization = organization;
                                break;
                            case "Нет":
                                break;
                            default:
                                break;
                        }
                        Console.WriteLine("Указать должность? Введите 'Да' или 'Нет'");
                        switch (Console.ReadLine())
                        {
                            case "Да":
                                string position = Console.ReadLine();
                                note_1.Position = position;
                                break;
                            case "Нет":
                                break;
                            default:
                                break;
                        }
                        notebook.AddNote(note_1, id_1);
                        Console.WriteLine("Запись добавлена.");
                        break;
                    case "Вывести все":
                        Dictionary<int, ShortNote> shortnotes = notebook.GetShortNotes();
                        foreach (KeyValuePair<int, ShortNote> id_and_note in shortnotes)
                        {
                            Console.WriteLine(id_and_note.Key + " "  + id_and_note.Value);
                        }
                        break;
                    case "Показать запись":
                        Console.WriteLine("Введите номер страницы: ");
                        int id_2 = Convert.ToInt32(Console.ReadLine());
                        Note note_2 = notebook.GetNote(id_2);
                        Console.WriteLine(note_2);
                        break;
                    case "Удалить запись":
                        Console.WriteLine("Какую страницу удалить? ");
                        int id_3 = Convert.ToInt32(Console.ReadLine());
                        notebook.DeleteNote(id_3);
                        break;
                    case "Редактировать запись":
                        Console.WriteLine("Введите имя: ");
                        string name_edit = Console.ReadLine();
                        Console.WriteLine("Введите фамилию: ");
                        string surname_edit = Console.ReadLine();
                        Console.WriteLine("Введите номер телефона: ");
                        long number_edit = Convert.ToInt64(Console.ReadLine());
                        Console.WriteLine("Введите страну: ");
                        string country_edit = Console.ReadLine();
                        Console.WriteLine("Введите страницу: ");
                        int id_edit = Convert.ToInt32(Console.ReadLine());
                        Note note_edit = new Note(surname_edit, name_edit, number_edit, country_edit);
                        notebook.EditNote(note_edit, id_edit);
                        Console.WriteLine("Запись обновлена.");
                        break;

                    default: Console.WriteLine("Вы ввели неправильную команду, повторите");
                        break;
                        
                }
            }
        }
    }
}

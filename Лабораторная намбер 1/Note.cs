﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лабораторная_намбер_1
{
    public class Note
    {
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }
        public long Number { get; set; }
        public string Country { get; set; }
        public DateTime Date_of_birth { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public string Notes { get; set; }

        public Note(string Surname, string Name, long Number, string Country)
            {
            this.Surname = Surname;
            this.Name = Name;
            this.Number = Number;
            this.Country = Country;
                        
            }
        public override string ToString()
        {
            
            return Surname+ " " + Name + " " + Number + " " + Patronymic + " " + Organization + " " + Position;
        }


    } 
}

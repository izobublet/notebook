﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лабораторная_намбер_1
{ // нужно ввести ID, Нужно ввести геттеры и сеттеры
    // сессионное хранение информации - использовать диктионари
    // 2-3 класса. 1 - запись, 2 - мэйн, 3 - визуализация 
    // исопльзовать динамический массив
    public class Notebook
    {
        Dictionary<int, Note> Note_Coll = new Dictionary<int, Note>();

        public Note AddNote(Note note, int id)
        {
            Note_Coll.Add(id, note);
            return note;
        }

        public Note EditNote(Note note, int id)
        {
            Note_Coll[id] = note;
            return note;
        }
        public void DeleteNote(int id)
        {
            Note_Coll.Remove(id);
        }
        public Note GetNote(int id)
        {
            return Note_Coll[id];
        }
        public Dictionary<int, ShortNote> GetShortNotes()
        {
           Dictionary<int, ShortNote> temp_dict = new Dictionary<int, ShortNote>();
            foreach (KeyValuePair<int,Note> idAndNote in Note_Coll)
            {
                Note note = idAndNote.Value;
                ShortNote shortnote = new ShortNote(note);
                temp_dict.Add(idAndNote.Key, shortnote);
            }
            return temp_dict;

        }


    }
}